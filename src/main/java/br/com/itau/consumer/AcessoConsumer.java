package br.com.itau.consumer;

import br.com.itau.acesso.models.Acesso;
import br.com.itau.io.CSVHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class AcessoConsumer {

    private static final Path path = Paths.get(System.getProperty("user.home"),"consumerLog.csv");

    @KafkaListener(topics = {"spec4-lucas-defante-1", "spec4-lucas-defante-2", "spec4-lucas-defante-3"}, groupId = "dev")
    public void receber(@Payload Acesso acesso) throws Exception {
        System.out.println("Acesso recebido: " + acesso.toString());
        CSVHandler.csvWriterOneByOne(acesso, path);
    }

}
