package br.com.itau.io;

import br.com.itau.acesso.models.Acesso;
import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class CSVHandler {

    public static void csvWriterOneByOne(Acesso acesso, Path path) throws Exception {
        File file = new File(path.toString());
        List<String[]> arquivoCsv = new ArrayList<>();
        if(file.exists()) {
            arquivoCsv = csvReadFile(path);
        } else {
            arquivoCsv = new ArrayList<>();
            arquivoCsv.add(acesso.getHeader());
        }
        CSVWriter writer = new CSVWriter(new FileWriter(path.toString()),
                                 ';',
                                         CSVWriter.NO_QUOTE_CHARACTER,
                                         CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                                         CSVWriter.DEFAULT_LINE_END);
        for (String[] linha : arquivoCsv) {
            writer.writeNext(linha);
        }
        for (String[] array : acesso.toList()) {
            writer.writeNext(array);
        }
        writer.close();
    }

    public static List<String[]> csvReadFile(Path path) throws Exception {
        List<String[]> arquivoCsv = new ArrayList<>();
        BufferedReader bufferedReader = Files.newBufferedReader(path, StandardCharsets.US_ASCII);
        String line = bufferedReader.readLine();
        String[] attributes;
        while (line != null) {
            attributes = line.split(";");
            arquivoCsv.add(attributes);
            line = bufferedReader.readLine();
        }
        return arquivoCsv;
    }
}
