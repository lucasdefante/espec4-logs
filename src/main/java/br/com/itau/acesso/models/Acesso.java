package br.com.itau.acesso.models;

import com.opencsv.bean.CsvBindByPosition;

import java.util.ArrayList;
import java.util.List;

public class Acesso {

    private int id;

    private int portaId;

    private int clienteId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    @Override
    public String toString() {
        return "Acesso{" +
                "id=" + id +
                ", portaId=" + portaId +
                ", clienteId=" + clienteId +
                '}';
    }

    public String[] getHeader() {
        return new String[] {"id", "portaId", "clienteId", "acessoLiberado"};
    }

    public List<String[]> toList() {
        List<String[]> list = new ArrayList<>();
        boolean acessoLiberado = false;
        if(id > 0) {
            acessoLiberado = true;
        }
        list.add(new String[] {String.valueOf(id), String.valueOf(portaId), String.valueOf(clienteId), String.valueOf(acessoLiberado)});
        return list;
    }
}
